Ange copy of QJson
==================

Copy of QJson from http://qjson.sourceforge.net/ modified to accept the
words 'Infinity' and 'NaN' as numbers.

QJson should allow this but has a bug that we have not found a good fix
for (https://github.com/flavio/qjson/issues/42), that is why we have
hacked the code to always allow special numbers.

The upstream README.md file has been moved to Upstream-README.md.

Upstream repository: https://github.com/flavio/qjson.git

Release
=======

As QJson has not been release for years but has developments on master
that we need use the date for last upstream commit as upstream version.

Example of version: 20140205-ange.1
